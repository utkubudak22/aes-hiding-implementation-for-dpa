/*
    @author: Utku Budak
    @TUM-Kennung: ge72quf
    @date: 2021 December
    This is the main file of the total CardOS implementation.
*/

#include <inttypes.h>
#include "aes_decrypt.h"
#include <stdio.h>

extern uint8_t key[16]; // Comment this while bringing the codes together
extern uint8_t buf[16]; // Comment this while bringing the codes together

int main()
{

    uint8_t *param;
    param = aes128_init(key);
 
	aes128_decrypt(buf, param);	// actual AES decryption

    for(uint8_t i = 0; i < 16; i++){
            printf("%02X", buf[i]);
    }

}
